﻿
using HW2_DB_Sber_ConsoleApp.Data;
using HW2_DB_Sber_ConsoleApp.Models;
using Microsoft.EntityFrameworkCore;

namespace DB_SberConsole
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** БД Сбер.ПикоБанк *****");

            SampleContextFactory contextFactory = new SampleContextFactory();
            using DBBankContext context = contextFactory.CreateDbContext(args);

            StartMigration(context);
            StartInitializationDB(context);

            while (true)
            {
                WriteConcoleMenu();

                string answer = Console.ReadLine();
                if (answer == "1")
                {
                    WriteConsoleTablesData(context);
                    
                }
                else if (answer == "2")
                {
                    AddCustumer(context);
                }
                else if (answer == "3")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Неизвестная команда");
                }

            }

        }

        static void StartMigration(DBBankContext context)
        {
            Console.WriteLine("Миграция данных...");
            context.Database.Migrate();
            Console.WriteLine("Миграция завершена.");
        }

        static void StartInitializationDB(DBBankContext context)
        {
            Console.WriteLine("Инициализация");
            DbInitializer.Initialize(context);
            Console.WriteLine("Инициализация завершена.");
        }

        static void WriteConcoleMenu()
        {
            Console.WriteLine("Меню:");
            Console.WriteLine("Вывести данные всех клиентов: [1]");
            Console.WriteLine("Добавить клиента: [2]");
            Console.WriteLine("Выйти: [3]");
            Console.Write("Выберите действие: ");
        }

        static void WriteConsoleTablesData(DBBankContext context)
         {
            Console.WriteLine("Данные клиентов:");
            Console.WriteLine();

            Console.WriteLine("Клиенты:");
            WriteConsoleTableData(context.Customers.ToList());
            Console.WriteLine();

            Console.WriteLine("Открытые счета клиентов:");
            WriteConsoleTableData(context.Accounts.ToList());
            Console.WriteLine();

            Console.WriteLine("Операции:");
            WriteConsoleTableData(context.Оperations.ToList());
            Console.WriteLine();

        }

        static void WriteConsoleTableData<T>(List<T> dataTable)
        {
            foreach (var item in dataTable)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void AddCustumer(DBBankContext context)
        {
            Console.WriteLine("Добавление клиента:");
            Console.Write("Фамилия: ");         string lastName = Console.ReadLine();
            Console.Write("Имя: ");             string firstName = Console.ReadLine();
            Console.Write("Отчество: ");        string secondName = Console.ReadLine();
            Console.Write("Дата рождения: ");   string birthday = Console.ReadLine();
            Console.Write("Номер телефона: ");  string phoneNumber = Console.ReadLine();
            Console.Write("Email: ");           string email = Console.ReadLine();

            if (firstName != null && secondName != null && lastName != null
                && DateOnly.TryParse(birthday, out DateOnly dateBrth) && email != null && phoneNumber != null)
            {
                Customer customer = new Customer() { FirstName = firstName, SecondName = secondName, LastName = lastName, Birthday = dateBrth, PhoneNumber = phoneNumber, Email = email };
                context.Customers.Add(customer);
                context.SaveChanges();
                Console.WriteLine("Клиент добавлен.");
            }
            else
            {
                Console.WriteLine("Введены некорректные данные, попробуйте еще раз!");
            }
        }

    }

}