﻿using HW2_DB_Sber_ConsoleApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2_DB_Sber_ConsoleApp.Data
{
    public static class DbInitializer
    {
        public static void Initialize(DBBankContext context)
        {

            if (context.Customers.Any())
            {
                return; 
            }

            var customers = new Customer[]
            {
                new Customer{FirstName="Carson",SecondName="John", LastName="Alexander", Birthday=DateOnly.Parse("1978-09-01"), PhoneNumber="+79933423523", Email="alex3523@yandex.ru"},
                new Customer{FirstName="Meredith",SecondName="Dan", LastName="Alonso", Birthday=DateOnly.Parse("1994-09-01"),   PhoneNumber="+78853423553", Email="jkec1454@gmail.com"},
                new Customer{FirstName="Arturo",SecondName="Elvis", LastName="Anand", Birthday=DateOnly.Parse("1974-09-01"),    PhoneNumber="+79323423576", Email="vandan@bk.ru"},
                new Customer{FirstName="Gytis",SecondName="David", LastName="Barzdukas", Birthday=DateOnly.Parse("2001-09-01"), PhoneNumber="+79943323023", Email="aers13@vk.ru" },
                new Customer{FirstName="Yan",SecondName="Mike", LastName="Li", Birthday=DateOnly.Parse("2002-09-01"),           PhoneNumber="+79053879523", Email="yand334@yandex.com" },
                new Customer{FirstName="Peggy",SecondName="Mary", LastName="Justice", Birthday=DateOnly.Parse("1997-09-01"),    PhoneNumber="+79933423783", Email="peg2423@gmail.com"},
                new Customer{FirstName="Laura",SecondName="Ann",LastName="Norman", Birthday=DateOnly.Parse("1985-09-01"),       PhoneNumber="+79333423353", Email="lua43@mail.ru"},
                new Customer{FirstName="Nino",SecondName="Li",LastName="Olivetto", Birthday=DateOnly.Parse("1967-09-01"),       PhoneNumber="+79033423529", Email="nin244@mail.ru"}
            };

            context.Customers.AddRange(customers);
            context.SaveChanges();

            var accounts = new Account[]
            {
                new Account{Number="88992444222242242412", Owner=customers[0] , Balance=500.00},
                new Account{Number="88992444222242242321", Owner=customers[1] , Balance=1500.00},
                new Account{Number="88992444222242242323", Owner=customers[2] , Balance=0.00},
                new Account{Number="88992444222242243525", Owner=customers[3] , Balance=2000.00},
                new Account{Number="88992444222242243523", Owner=customers[4] , Balance=1000.00},
                new Account{Number="88992444222242352532", Owner=customers[5] , Balance=200.00},
                new Account{Number="88992444222242342535", Owner=customers[6] , Balance=300.00},
                new Account{Number="88992444222242352335", Owner=customers[7] , Balance=0.00}
            };

            context.Accounts.AddRange(accounts);
            context.SaveChanges();

            var operations = new Оperation[]
            {
                new Оperation{Account=accounts[1], Sum=-500.00, Date=DateOnly .Parse("2022-01-01"), Description="Снятие наличных"},
                new Оperation{Account=accounts[3], Sum=200.00, Date=DateOnly .Parse("2022-02-01"), Description="Внесение наличных"},
                new Оperation{Account=accounts[4], Sum=-500.00, Date=DateOnly .Parse("2022-05-22"), Description="Снятие наличных"},
                new Оperation{Account=accounts[5], Sum=1500.00, Date=DateOnly .Parse("2022-02-01"), Description="Внесение наличных"},
                new Оperation{Account=accounts[6], Sum=-1200.00, Date=DateOnly .Parse("2022-01-01"), Description="Снятие наличных"},
                new Оperation{Account=accounts[7], Sum=500.00, Date=DateOnly .Parse("2022-02-11"), Description="Внесение наличных"},
                new Оperation{Account=accounts[0], Sum=-500.00, Date=DateOnly .Parse("2022-01-01"), Description="Снятие наличных"},
                new Оperation{Account=accounts[0], Sum=500.00, Date=DateOnly .Parse("2022-03-15"), Description="Внесение наличных"},
                new Оperation{Account=accounts[1], Sum=-400.00, Date=DateOnly .Parse("2022-01-01"), Description="Снятие наличных"},
                new Оperation{Account=accounts[2], Sum=500.00, Date=DateOnly .Parse("2022-04-07"), Description="Внесение наличных"},
                new Оperation{Account=accounts[3], Sum=-600.00, Date=DateOnly .Parse("2022-01-01"), Description="Снятие наличных"},
                new Оperation{Account=accounts[4], Sum=500.00, Date=DateOnly .Parse("2022-05-01"), Description="Внесение наличных"},
                new Оperation{Account=accounts[5], Sum=-700.00, Date=DateOnly .Parse("2022-01-13"), Description="Снятие наличных"},
                new Оperation{Account=accounts[6], Sum=500.00, Date=DateOnly .Parse("2022-03-30"), Description="Внесение наличных"},

            };

            context.Оperations.AddRange(operations);
            context.SaveChanges();

        }

    }
}
