﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2_DB_Sber_ConsoleApp.Models
{
    public class Customer
    {
        public int ID { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string SecondName { get; set; }
        [Required]
        public DateOnly Birthday { get; set; }
        [Required]
        [MaxLength(20)]
        public string PhoneNumber { get; set; }
        [Required]
        [MaxLength(30)]
        public string Email { get; set; }

        public override string ToString()
        {
            return $"ID = {ID}, FirstName = {FirstName} , SecondName = {SecondName}, LastName = {LastName}, Birthday = {Birthday}, PhoneNumber = {PhoneNumber}, Email = {Email ?? "(не указан)"};";
        }

    }
}
